from django.shortcuts import render
from django.http import request,response
from django.http import HttpResponseRedirect,HttpResponseNotFound,Http404,HttpResponse
from django.urls import reverse
from django.views import generic

import urllib
import http.client
import json

def get_item_json(item_val):
    #conn=http.client.HTTPSConnection("hacker-news.firebaseio.com")
    item_string='/v0/item/'+str(item_val)+'.json'
    #conn.request("GET",item_string)
    #result=conn.getresponse()
    #data=json.loads(result.read())
    #conn.close()
    url='https://hacker-news.firebaseio.com'+item_string
    r=urllib.request.urlopen(url)
    data=json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
    return data

def get_top_stories(request):
    conn=http.client.HTTPSConnection("hacker-news.firebaseio.com")
    conn.request("GET","/v0/topstories.json")
    result=conn.getresponse()
    data=json.loads(result.read())
    stories=[]
    for i in range(30):
        item_string='/v0/item/'+str(data[i])+'.json'
        conn.request("GET",item_string)
        result=conn.getresponse()
        data2=json.loads(result.read())
        stories.append(data2)
    conn.close()

    return render(request,'hackernews/topstories.html',{'top_stories':stories})


def get_discussion(request,story_id):
    conn=http.client.HTTPSConnection("hacker-news.firebaseio.com")

    item_string='/v0/item/'+str(story_id)+'.json'
    conn.request("GET",item_string)
    result=conn.getresponse()
    data=json.loads(result.read())
    conn.close()

    visited=[]
    comments=data['kids'].copy()
    
    index=-1
    for num in data['kids']:
        index=index+1
        tmp_data=get_item_json(num)
        visited.append(num)
        try:
            comments[index+1:len(tmp_data['kids'])]=tmp_data['kids']
        except:
            pass
    
    #second iteration
    index=-1
    for num in comments:
        index=index+1
        if num not in visited:
            tmp_data=get_item_json(num)
            visited.append(num)
            try:
                comments[index+1:len(tmp_data['kids'])]=tmp_data['kids']
            except:
                pass
    #third iteration
    index=-1
    for num in comments:
        index=index+1
        if num not in visited:
            tmp_data=get_item_json(num)
            visited.append(num)
            try:
                comments[index+1:len(tmp_data['kids'])]=tmp_data['kids']
            except:
                pass
    #fourth iteration
    index=-1
    for num in comments:
        index=index+1
        if num not in visited:
            tmp_data=get_item_json(num)
            visited.append(num)
            try:
                comments[index+1:len(tmp_data['kids'])]=tmp_data['kids']
            except:
                pass
    index=-1
    for num in comments:
        index=index+1
        tmp_data=get_item_json(num)
        try:
            comments[index]=tmp_data['text']
        except:
            del comments[index]
    data['comments']=comments.copy()

    return render(request,'hackernews/discussion.html',{'story':data})

def get_user(request,user_id):
    item_string='/v0/user/'+ user_id +'.json'
    conn=http.client.HTTPSConnection("hacker-news.firebaseio.com")
    conn.request("GET",item_string)
    result=conn.getresponse()
    data=json.loads(result.read())
    conn.close()
    return render(request,'hackernews/userdetail.html',{'user':data})
