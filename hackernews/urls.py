from django.urls import path
from . import views

urlpatterns = [
    path('',views.get_top_stories,name='topstories'),
    path('discussion/<slug:story_id>/',views.get_discussion,name='discussion'),
    path('user/<slug:user_id>/',views.get_user,name='user'),
]